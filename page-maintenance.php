<?php get_header('maintenance'); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main">
    <div class="row">
        <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
        <section class="maintenance col-md-12 page-content" style="background: url(<?php echo $url; ?>); background-size: cover;">
            <div class="container">
                <div class="row">
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-footer.png" alt="Lluvia de Obsequios" />
                        <?php the_content(); ?>
                    </article>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer('maintenance'); ?>
