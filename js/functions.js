var shareShow = false;
var wishShow = false;
var frontLogin = false;
$(function () {
    "use strict";
    if ($("#contact-form").length > 0) {
        $("#contact-form").validationEngine('attach', {
            scroll: false,
            onValidationComplete: function (form, status) {
                if (status === true) {
                    $('#form-submit-wrapper').empty();
                    $('#form-submit-wrapper').addClass('loading');
                    var form_data = $('#contact-form').serialize();
                    $('#btn_submit').attr({disabled: 'disabled', value: 'ENVIANDO...'});
                    $.post('../contacto-submit', form_data, function (data) {
                        $('#form-submit-wrapper').html(data);
                        $('#form-submit-wrapper').removeClass('loading');
                        $('#btn_submit').attr({disabled: 'disabled', value: 'LISTO!'});
                    });
                }
            }
        });
    }
});

$(document).ready(function () {
    "use strict";
    $("#billing_RIF").inputmask({mask: "a999999999"});
    $("#sticker").sticky({topSpacing: 0});
    $("#search").validationEngine();
    $("body").niceScroll({
        cursorcolor: '#BE1B20',
        cursorborder: '0px',
        cursorwidth: '10px',
        cursorborderradius: '0px',
        background: '#000000',
        hwacceleration: false,
        scrollspeed: 60,
        mousescrollstep: 85,
        smoothscroll: true,
        autohidemode: false,
        zindex: '99999'
    });
    $('.front-gallery-prods').flickity({
        // options
        imagesLoaded: true,
        cellAlign: 'left',
        freeScroll: true,
        autoPlay: true,
        wrapAround: true,
        pageDots: false
    });
    $(".related-products").owlCarousel({
        items: 4,
        pagination: false,
        navigation: true,
        navigationText: ["<span class='sprite-slider sprite-slider-prev'></span>", "<span class='sprite-slider sprite-slider-next'></span>"]
    });
    $("#billing_state_field label").html('<label for="billing_state" class="">Estado <abbr class="required" title="obligatorio">*</abbr></label>');
    $("#fecha_field").datepicker({ minDate: '+2D', maxDate: '+45D' }, $.datepicker.regional["es"]);
});

$('.front-featured-prods-title').lettering('words');

$('#btn-front-login').click(function () {
    "use strict";
    if (frontLogin === false) {
        $('.front-docked-login').removeClass('front-docked-login-hide');
        $('.front-docked-login').addClass('front-docked-login-show');
        frontLogin = true;
    } else {
        $('.front-docked-login').addClass('front-docked-login-hide');
        $('.front-docked-login').removeClass('front-docked-login-show');
        frontLogin = false;
    }
});

$('#share-btn').click(function () {
    "use strict";
    if ($('.yith-wcwl-wishlistexistsbrowse').hasClass('show')) {
        wishShow = true;
    }
    if (shareShow === false) {
        $('#special-share').addClass('show-share');
        $('#special-share').removeClass('hide-share');
        if (wishShow === true) {
            $('.yith-wcwl-wishlistexistsbrowse').removeClass('show');
            $('.yith-wcwl-wishlistexistsbrowse').addClass('hide');
        }
        shareShow = true;
    } else {
        $('#special-share').removeClass('show-share');
        $('#special-share').addClass('hide-share');
        if (wishShow === true) {
            $('.yith-wcwl-wishlistexistsbrowse').addClass('show');
            $('.yith-wcwl-wishlistexistsbrowse').removeClass('hide');
        }
        shareShow = false;
    }

});

$('.single_add_to_wishlist').click(function () {
    "use strict";
    if (shareShow === true) {
        $('#special-share').removeClass('show-share');
        $('#special-share').addClass('hide-share');
        shareShow = false;
    }
    setTimeout(function () {
        $.ajax({
            type: 'POST',
            url: ruta_blog + '/inc/wishlist-function.php',
            success: function (resp) {
                setTimeout(function () {
                    $('#wishlist-btn').html(resp);
                }, 1000);
            },
            error: function () {
                alert('error');
            }
        }); }, 1500);
});

$('.remove_from_wishlist').click(function () {
    "use strict";
    setTimeout(function () {
        $.ajax({
            type: 'POST',
            url: ruta_blog + '/inc/wishlist-function.php',
            success: function (resp) {
                setTimeout(function () {
                    $('#wishlist-btn').html(resp);
                }, 1000);
            },
            error: function () {
                alert('error');
            }
        }); }, 2500);
});

wow = new WOW(
    {
        boxClass:     'wow',      // default
        animateClass: 'animated', // default
        offset:       150,          // default
        mobile:       true,       // default
        live:         true        // default
    }
);
wow.init();

smoothScroll.init({
    selector: '[data-scroll]', // Selector for links (must be a valid CSS selector)
    selectorHeader: '[data-scroll-header]', // Selector for fixed headers (must be a valid CSS selector)
    speed: 500, // Integer. How fast to complete the scroll in milliseconds
    easing: 'easeInOutCubic', // Easing pattern to use
    updateURL: true, // Boolean. Whether or not to update the URL with the anchor hash on scroll
    offset: 190, // Integer. How far to offset the scrolling anchor location in pixels
    scrollOnLoad: true, // Boolean. If true, animate to anchor on page load if URL has a hash
    callback: function (toggle, anchor) {} // Function to run after scrolling
});

