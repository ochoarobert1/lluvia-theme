<?php
if ($_SERVER['REMOTE_ADDR'] == '::1') {
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp_lluvia/wp-load.php' );
} else {
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );
}
global $wp_query;

if ( is_user_logged_in() ) { ?>
<?php global $wpdb; $current_user = wp_get_current_user(); ?>
<?php $wish_count = $wpdb->get_var( $wpdb->prepare("SELECT COUNT(*) FROM wp_yith_wcwl WHERE user_id = %s", $current_user->ID)); ?>
<?php } else { ?>
<?php $cookie = $_COOKIE['yith_wcwl_products']; ?>
<?php $cookie = stripslashes($cookie); $variable = json_decode($cookie, true); ?> 
<?php $wish_count = count($variable); }?>
<span class="sprites-wishcart sprites-wish">
    <?php if ($wish_count != 0) { ?>
    <span class="wishcart-badge animated fadeIn"><?php echo $wish_count; ?></span>
    <?php } ?>
</span>
