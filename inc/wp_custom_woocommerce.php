<?php
/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

/* REMOVE DEFAULT WRAPPER */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

/* ADD CUSTOM WRAPPER */
add_action('woocommerce_before_main_content', 'lluvia_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'lluvia_wrapper_end', 10);

function lluvia_wrapper_start() {
    echo '<section id="main" class="container-fluid"><div class="row"><div class="col-md-12">';
}

function lluvia_wrapper_end() {
    echo '</div></div></section>';
}

/* ADD VENEZUELAN CURRENCY */
add_filter( 'woocommerce_currencies', 'add_my_currency' );

function add_my_currency( $currencies ) {
    $currencies['VEF'] = __( 'Bolivar Venezolano', 'lluvia' );
    return $currencies;
}

add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);

function add_my_currency_symbol( $currency_symbol, $currency ) {
    switch( $currency ) {
        case 'VEF': $currency_symbol = 'BsF'; break;
    }
    return $currency_symbol;
}

/* REMOVE BREADCRUMBS */
remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);

/* SHOP CUSTOM */
remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);

add_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_price', 10);
add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);

/* SINGLE CUSTOM */
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 50);




/* ADD THEME SUPPORT */
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}



add_filter('woocommerce_product_get_rating_html', 'your_get_rating_html', 10, 2);

function your_get_rating_html($rating_html, $rating) {
    if ( $rating > 0 ) {
        $title = sprintf( __( 'Rated %s out of 5', 'woocommerce' ), $rating );
    } else {
        $title = 'Not yet rated';
        $rating = 0;
    }

    $rating_html  = '<div class="star-rating" title="' . $title . '">';
    $rating_html .= '<span style="width:' . ( ( $rating / 5 ) * 100 ) . '%"><strong class="rating">' . $rating . '</strong> ' . __( 'out of 5', 'woocommerce' ) . '</span>';
    $rating_html .= '</div>';

    return $rating_html;
}



/**
 * Add the field to the checkout
 */

add_filter( 'woocommerce_checkout_fields' , 'custom_override_order_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_order_fields( $fields ) {
    $fields['billing']['billing_email']['label'] = 'Dirección de Correo Electrónico';
    $fields['shipping']['shipping_first_name']['label'] = 'De:';
    $fields['shipping']['shipping_last_name']['label'] = 'Para:';
    $fields['shipping']['shipping_company']['label'] = 'Persona que recibe:';
    $fields['shipping']['shipping_company']['required'] = 'true';
    $fields['order']['order_comments']['maxlength'] = '300';
    $fields['order']['order_comments']['label'] = 'Notas del Pedido / Puntos de Referencia';
    return $fields;
}

add_action( 'woocommerce_after_order_notes', 'my_custom_checkout_field' );

function my_custom_checkout_field( $checkout ) {

    echo '<div id="my_custom_checkout_field">';

    woocommerce_form_field( 'dedicatoria_field', array(
        'type'          => 'textarea',
        'maxlength'          => '300',
        'class'         => array('my-field-class form-row-wide'),
        'label'         => __('Escribe tu dedicatoria'),
        'placeholder'   => __('Ingrese una dedicatoria para el regalo'),
    ), $checkout->get_value( 'factura_field' ));

    woocommerce_form_field( 'fecha_field', array(
        'type'          => 'text',
        'class'         => array('my-field-class2 form-row-wide'),
        'label'         => __('Fecha de Entrega'),
    ), $checkout->get_value( 'factura_field' ));

    echo '</div>';

}

// Hook in
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields', 100 );
function custom_override_checkout_fields( $fields ) {
    $fields['shipping']['shipping_phone'] = array(
        'label'     => __('Teléfono de quien recibe', 'woocommerce'),
        'placeholder'   => _x('', 'placeholder', 'woocommerce'),
        'required'  => false,
        'class'     => array('form-row-wide'),
        'clear'     => true
    );
    $fields['billing']['billing_custom'] = array(
        'type' => 'select',
        'label'     => __('¿Como desea recibir su factura?', 'lluvia'),
        'required'  => false,
        'class'     => array('form-row-wide input-text '),
        'clear'     => true
    );
    $fields['billing']['billing_custom']['options'] = array(
        'Impresa' => 'Impresa',
        'Digital' => 'Digital'
    );
    $fields['billing']['billing_RIF'] = array(
        'type'      => 'text',
        'label'     => __('Cédula / Rif', 'lluvia'),
        'placeholder'   => _x('Ingrese su Cédula o RIF - Formato: V123456789 / J1234567890', 'lluvia'),
        'required'  => false,
        'class'     => array('form-row-wide input-text '),
        'clear'     => true
    );

    $fields['billing']['billing_state']['label'] = 'Estado';
    return $fields;
}

add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');

function my_custom_checkout_field_process() {
    // Check if set, if its not set add an error.
    if ( ! $_POST['billing_RIF'] )
        wc_add_notice( __( 'Debe ingresar su Cédula o RIF.' ), 'error' );
}

/**
 * Update the order meta with field value
 */
add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );

function my_custom_checkout_field_update_order_meta( $order_id ) {
    if ( ! empty( $_POST['dedicatoria_field'] ) ) {
        update_post_meta( $order_id, 'dedicatoria_field', sanitize_text_field( $_POST['dedicatoria_field'] ) );
    }
    if ( ! empty( $_POST['fecha_field'] ) ) {
        update_post_meta( $order_id, 'fecha_field', sanitize_text_field( $_POST['fecha_field'] ) );
    }
}

/**
 * Display field value on the order edit page
 */
add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

function my_custom_checkout_field_display_admin_order_meta($order){
    $factura_field = get_post_meta( $order->id, '_billing_custom', true );
    echo '<p><strong>'.__('Cédula / RIF').':</strong> ' . get_post_meta( $order->id, '_billing_RIF', true ) . '</p>';
    echo '<p><strong>'.__('El Cliente desea su factura en').':</strong> ' . $factura_field . '</p>';
    echo '<p><strong>'.__('Dedicatoria para regalo').':</strong> ' . get_post_meta( $order->id, 'dedicatoria_field', true ) . '</p>';
    echo '<p><strong>'.__('Fecha de Entrega').':</strong> ' . get_post_meta( $order->id, 'fecha_field', true ) . '</p>';
}
add_filter('woocommerce_add_cart_item_data','namespace_force_individual_cart_items',10,2);
function namespace_force_individual_cart_items($cart_item_data, $product_id)
{
    $unique_cart_item_key = md5(microtime().rand()."Lluvia");
    $cart_item_data['unique_key'] = $unique_cart_item_key;

    return $cart_item_data;
}


add_filter( 'woocommerce_component_options_per_page', 'wc_cp_component_options_per_page', 10, 3 );
function wc_cp_component_options_per_page( $results_count, $component_id, $composite ) {
    $results_count = 12;
    return $results_count;
}

// Creating the widget
class wpb_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
            // Base ID of your widget
            'wpb_widget',

            // Widget name will appear in UI
            __('Woocoommerce Categorias Sidebar', 'lluvia'),

            // Widget description
            array( 'description' => __( 'Categorias de Woocommerce', 'lluvia' ), )
        );
    }

    // Creating widget front-end
    // This is where the action happens
    public function widget( $args, $instance ) {
        $title = apply_filters( 'widget_title', $instance['title'] );
        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if ( ! empty( $title ) )
            echo $args['before_title'] . $title . $args['after_title'];

        // This is where you run the code and display the output

?>
<div class="sidebar-categories">
    <?php $terminos = array ('corporativo', 'arma-tu-cesta'); ?>
    <?php $termid = array(); ?>
    <?php foreach ($terminos as $term) { ?>
    <?php $terms = get_term_by( 'slug', $term, 'product_cat'); ?>
    <?php $termid[] = $terms->term_id; ?>
    <?php } ?>
    <?php
        // no default values. using these as examples
        $taxonomies = array(
            'product_cat'
        );

        $args = array(
            'menu_order'        => 'asc',
            'hide_empty'        => true,
            'exclude'           => $termid,
            'exclude_tree'      => array(),
            'include'           => array(),
            'number'            => '',
            'fields'            => 'all',
            'slug'              => '',
            'parent'            => 0,
            'hierarchical'      => true,
            'child_of'          => 0,
            'childless'         => false,
            'get'               => '',
            'name__like'        => '',
            'description__like' => '',
            'pad_counts'        => false,
            'offset'            => '',
            'search'            => '',
            'cache_domain'      => 'core'
        );

        $terms = get_terms($taxonomies, $args); ?>
    <?php if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) { $i = 1; ?>
    <?php foreach ( $terms as $term ) { ?>
    <div class="panel-group" id="accordion<?php echo $i; ?>" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default panel-custom">
            <div class="panel-heading" role="tab" id="<?php echo $term->slug; ?>">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion<?php echo $i; ?>" href="#<?php echo $term->slug; ?>-collapse" aria-expanded="true" aria-controls="<?php echo $term->slug; ?>-collapse">
                        <?php echo $term->name; ?>
                    </a>
                </h4>
            </div>
            <div id="<?php echo $term->slug; ?>-collapse" class="panel-collapse collapse" role="tabpanel" aria-labelledby="<?php echo $term->slug; ?>">
                <div class="panel-body">
                    <?php $taxonomy_name = array( 'product_cat' ); ?>
                    <?php $posts_array = get_term_children( $term->term_id, 'product_cat' );  ?>
                    <?php foreach ( $posts_array as $child ) { ?>
                    <?php $term2 = get_term_by( 'id', $child, 'product_cat' ); ?>
                    <?php $link = get_term_link($term2->term_id, 'product_cat'); ?>
                    <p><a href="<?php if (is_wp_error($link)) { } else { echo $link; } ?>"><?php echo $term2->name; ?></a></p>
                    <?php } ?>
                    <p><a href="<?php echo get_term_link($term->term_id, 'product_cat'); ?>">Ver todos</a></p>
                </div>
            </div>
        </div>
    </div>
    <?php } $i++; } ?>
</div>
<?php echo $args['after_widget'];
    }

    // Widget Backend
    public function form( $instance ) {
        if ( isset( $instance[ 'title' ] ) ) {
            $title = $instance[ 'title' ];
        }
        else {
            $title = __( 'New title', 'lluvia' );
        }
        if ( isset( $instance[ 'categories_ex' ] ) ) {
            $categories_ex = $instance[ 'categories_ex' ];
        }
        else {
            $categories_ex = __( 'Nueva Categoria', 'lluvia' );
        }
        // Widget admin form
?>
<p>
    <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<p>
    <label for="<?php echo $this->get_field_id( 'categories_ex' ); ?>"><?php _e( 'Categorias a Excluir:' ); ?></label>
    <input class="widefat" id="<?php echo $this->get_field_id( 'categories_ex' ); ?>" name="<?php echo $this->get_field_name( 'categories_ex' ); ?>" type="text" value="<?php echo esc_attr( $categories_ex ); ?>" />
</p>
<?php
    }

    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        $instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
        return $instance;
    }
} // Class wpb_widget ends here

// Register and load the widget
function wpb_load_widget() {
    register_widget( 'wpb_widget' );
}
add_action( 'widgets_init', 'wpb_load_widget' );



add_action( 'pre_get_posts', 'custom_pre_get_posts_query' );

function custom_pre_get_posts_query( $q ) {
    if ( ! $q->is_main_query() ) return;
    if ( ! $q->is_post_type_archive() ) return;

    if ( ! is_admin() && is_shop() ) {

        $q->set( 'tax_query', array(array(
            'taxonomy' => 'product_cat',
            'field' => 'slug',
            'terms' => array( 'corporativo', 'arma-tu-cesta' ), // Don't display products in the knives category on the shop page
            'operator' => 'NOT IN'
        )));
    }

    remove_action( 'pre_get_posts', 'custom_pre_get_posts_query' );

}

/* FUNCTION QUERY SEPARADOS - CUSTOM WOOCOMMERCE SEARCH FORM */
function multiplevars($query_var){
    global $wpdb;
    $str = $query_var;
    $contador = strlen($str);
    if ($contador > 4) {
        $cadena = substr($str, 0, -2);
    } else {
        $cadena = $str;
    }
    $mypostids = $wpdb->get_col("select ID from $wpdb->posts where post_title LIKE '%".$cadena."%' ");
    return $mypostids;
}





add_action( 'woocommerce_after_add_to_cart_form', 'custom_share_content', 90);

function custom_share_content( ) {
    echo '<a id="share-btn" class="share-btn"><img src="' . esc_url(get_template_directory_uri()) . '/images/share-icon.png" alt=""></a>';
}

function woo_related_products_limit() {
    global $product;

    $args['posts_per_page'] = 8;
    return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
function jk_related_products_args( $args ) {
    $args['posts_per_page'] = 8; // 4 related products
    return $args;
}

add_filter( 'woocommerce_cart_needs_shipping', '__return_true' );
