<?php get_header('shop'); ?>
<?php the_post(); ?>
<?php $defaultatts = array ('class' => 'img-responsive'); ?>
<section class="pre-categories col-md-12 no-paddingl no-paddingr">
    <div class="container">
        <div class="row">
            <div class="pre-categories-cesta col-md-12">
                <?php $args = array ('post_type' => 'product', 'posts_per_page' => -1, 'tax_query' => array ( array ('taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => 'arma-tu-cesta', 'include_children' => false))); ?>
                <?php query_posts($args)?>
                <?php while (have_posts()) : the_post(); ?>
                <?php $terms = get_the_terms(get_the_ID(), 'product_cat'); ?>
                <?php $max = sizeof($terms); ?>
                <?php if ($max <= 1) { ?>
                <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                    <?php the_post_thumbnail('avatar',$defaultatts); ?>
                </a>
                <?php } ?>
                <?php endwhile; wp_reset_query();?>
            </div>
        </div>
    </div>
    <div class="pre-category-item-title col-md-12">
        <?php echo '<h2 class="page-title">' . get_the_title() . '</h2>'; ?>
        <h3>Elige el motivo de tu cesta</h3>
        <h4>(Servicio disponible solo para Caracas)</h4>
    </div>
</section>
<main class="container-fluid" role="main">
    <div class="row">
        <section class="col-md-12 page-content">
            <div class="container">
                <div class="row">
                    <article id="post-<?php the_ID(); ?>" class="col-md-12 <?php echo join(' ', get_post_class()); ?>">
                        <?php the_content(); ?>
                    </article>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
