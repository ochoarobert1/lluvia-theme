<footer class="container-fluid" role="contentinfo">
    <div class="row">
        <div class="the-footer col-md-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="footer-contact col-md-3 col-sm-3 col-xs-6">
                        <h5>CONTÁCTANOS</h5>
                        <div class="footer-contact-item">
                            <div class="col-md-2 col-sm-3 col-xs-3 no-paddingl">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/telf.png" alt="Teléfonos" />
                            </div>
                            <div class="col-md-10 col-sm-9 col-xs-9 no-paddingl">
                                <p>+58 (212) 619.29.48</p>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="footer-contact-item">
                            <div class="col-md-2 col-sm-3 col-xs-3 no-paddingl">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/email.png" alt="Correo Electrónico" />
                            </div>
                            <div class="col-md-10 col-sm-9 col-xs-9 no-paddingl">
                                info@lluviadeobsequios.com <br />cliente@lluviadeobsequios.com
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-2 col-xs-6">
                        <h5>INFORMACIÓN</h5>
                        <ul>
                            <li><a href="<?php echo home_url('/mi-cuenta'); ?>">Mi Cuenta</a></li>
                            <li>Pedidos</li>
                            <li>Favoritos</li>
                            <li>Devoluciones</li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6">
                        <h5>PREGUNTAS FRECUENTES</h5>
                        <div class="footer-faq col-md-12 no-paddingl no-paddingr">
                            <?php wp_nav_menu(array( 'theme_location' => 'footer_menu', 'walker' => new themeslug_walker_nav_menu)); ?>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <h5>¿QUIENES SÓMOS?</h5>
                        <ul>
                            <li><a href="<?php echo home_url('/la-empresa'); ?>">La Empresa</a></li>
                            <li><a href="<?php echo home_url('/terminos-y-condiciones'); ?>">Términos y Condiciones</a></li>
                        </ul>
                        <div class="footer-img col-md-12 no-paddingl no-paddingr">
                            <div class="col-md-5 col-sm-5 col-xs-5 no-paddingl">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-footer.png" alt="Lluvia de Obsequios" class="img-responsive" />
                            </div>
                            <div class="col-md-7 col-sm-7 col-xs-7">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-fiocco.png" alt="Lluvia de Obsequios" class="img-responsive" />
                            </div>

                        </div>
                    </div>
                    <div class="ssl-seal col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/cert.png" alt="" class="img-responsive" />
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copy col-md-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <p>&copy; 2015 FIOCCO DI NEVE C.A. RIF. J-40547816-0. TODOS LOS DERECHOS RESERVADOS</p>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <p>DESARROLLADO POR <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-dlk.png" alt="" class="img-responsive"/></p>
                    </div>
                    <div class="gotop col-md-2">
                        <a data-scroll href="#top" title="Volver al inicio">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/gotop.png" alt="volver al inicio" />
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>
</body>
</html>
