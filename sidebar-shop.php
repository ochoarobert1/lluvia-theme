<?php if ( is_active_sidebar( 'shop_sidebar' ) ) : ?>
<ul id="shop-sidebar" class="shop-sidebar">
    <?php dynamic_sidebar( 'shop_sidebar' ); ?>
</ul>
<?php endif; ?>
