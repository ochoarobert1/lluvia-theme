<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main">
    <div class="row">
        <section class="the-slider col-md-12 col-sm-12 col-xs-12 ">
            <?php the_content();  ?>
        </section>
        <?php if (is_user_logged_in()) { } else { ?>
        <section class="front-login col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
                        <a id="btn-front-login"><button class="btn btn-md btn-front-login">INICIAR SESIÓN</button></a>
                        <a href="<?php echo home_url('/mi-cuenta'); ?>"><button class="btn btn-md btn-front-login">REGISTRO</button></a>
                    </div>
                    <div class="clearfix"></div>
                    <div class="front-docked-login front-docked-login-hide col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
                        <?php $args = array( 'label_username' => __( 'Nombre de Usuario' ), 'label_password' => __( 'Contraseña' ), 'label_remember' => __( 'Recuerdame?' ), 'label_log_in'   => __( 'Iniciar Sesión' )); ?>
                        <?php wp_login_form( $args ); ?>
                    </div>
                </div>
            </div>
        </section>
        <?php } ?>
        <section class="front-promo col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                        <?php $args = array('post_type' => 'promociones', 'posts_per_page' => 1, 'order' => 'ASC', 'orderby' => 'date' ); ?>
                        <?php query_posts($args); ?>
                        <?php while (have_posts()) : the_post(); ?>
                        <?php $group_values = rwmb_meta( 'promo_group' ); ?>
                        <?php
                        if ( ! empty( $group_values ) ) {
                            $i = 1;
                            foreach ( $group_values as $group_value ) { ?>
                        <?php $value1 = isset( $group_value['link_promo'] ) ? $group_value['link_promo'] : ''; ?>
                        <?php $value2 = isset( $group_value['banner_promo'] ) ? $group_value['banner_promo'] : ''; ?>
                        <?php if (($i == 1) || ($i == 3)) { ?>
                        <div class="col-md-3 col-sm-3 col-xs-3 animated wow fadeIn">
                            <a href="<?php echo esc_url($value1); ?>"><img src="<?php echo esc_url($value2); ?>" alt="" class="img-responsive" /></a>
                        </div>
                        <?php } else { ?>
                        <div class="col-md-6 col-sm-6 col-xs-6 animated wow fadeIn">
                            <a href="<?php echo esc_url($value1); ?>"><img src="<?php echo esc_url($value2); ?>" alt="" class="img-responsive" /></a>
                        </div>
                        <?php } $i++; } } ?>
                        <?php endwhile; wp_reset_query(); ?>

                    </div>
                </div>
            </div>
        </section>
        <section class="search-cat col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="front-search col-md-12">
                        <div class="col-md-8 col-md-offset-2 col-sm-12">
                            <h2 class="front-search-title">ENCUENTRA TUS OBSEQUIOS</h2>
                            <form action="<?php echo home_url('/'); ?>" class="form-inline">
                                <input type="text" name="s" id="s" class="form-control" />
                                <input type="hidden" name="post_type" id="post_type" class="form-control" value="product" />
                                <button type="submit" class="btn btn-md btn-front-search">BUSCAR</button>
                            </form>
                        </div>
                    </div>
                    <div class="front-categories col-md-12 col-sm-12 col-xs-12">
                        <?php
                        // no default values. using these as examples
                        $taxonomies = array( 'product_cat' );
                        $args = array('menu_order' => 'asc', 'hide_empty' => false, 'parent' => 0 );  ?>
                        <?php $terms = get_terms( $taxonomies, $args ); ?>
                        <?php if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){ ?>
                        <?php foreach ( $terms as $term ) { ?>

                        <div class="front-category-item col-md-5ths col-xs-5ths col-sm-5ths">
                            <?php if ($term->name == "Corporativo") { $link = home_url('/corporativo'); } else if ($term->slug == "arma-tu-cesta") { $link = home_url('/tienda/arma-tu-cesta'); } else { $link = get_term_link($term); }?>
                            <a href="<?php echo $link; ?>">
                                <?php $thumbnail_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true ); ?>
                                <?php $image = wp_get_attachment_url( $thumbnail_id ); ?>
                                <?php if ( $image ) { echo '<img src="' . $image . '" alt="" class="img-responsive"/>'; } ?>
                            </a>
                            <h5><?php echo $term->name ?></h5>
                        </div>
                        <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="front-featured-prods col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row visible-md visible-lg">
                    <div class="col-md-12 col-sm-12 col-xs-12  no-paddingl no-paddingr">
                        <h2 class="front-featured-prods-title">PRODUCTOS DESTACADOS</h2>
                        <div class="front-gallery-prods col-md-12 col-sm-12 col-xs-12  no-paddingl no-paddingr">
                            <?php $args = array ('post_type' => 'product', 'posts_per_page' => 16, 'tax_query' => array( array( 'taxonomy'  => 'product_cat', 'field'     => 'slug', 'terms' => 'arma-tu-cesta', 'operator'  => 'NOT IN'))); ?>
                            <?php query_posts($args); $i=1; ?>
                            <?php while (have_posts()): the_post(); ?>
                            <?php if ($i == 1) { echo '<div class="gallery-cell col-md-12 col-sm-12 col-xs-12">'; } ?>
                            <div class="col-md-3 col-sm-6 col-xs-6 <?php echo join(' ', get_post_class()); ?>">
                                <div class="shop-product-single-item col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                    <?php do_action( 'woocommerce_before_shop_loop_item' ); ?>

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <?php
                                        /**
                 * woocommerce_before_shop_loop_item_title hook
                 *
                 * @hooked woocommerce_show_product_loop_sale_flash - 10
                 * @hooked woocommerce_template_loop_product_thumbnail - 10
                 */
                                        do_action( 'woocommerce_before_shop_loop_item_title' ); ?>
                                    </div>
                                    <div class="shop-product-single-item-price col-md-12 col-sm-12 col-xs-12">
                                        <?php
                                        /**
                * Aqui Esta el Price
                * woocommerce_shop_loop_item_title hook
                *
                * @hooked woocommerce_template_loop_product_title - 10
                */
                                        do_action( 'woocommerce_shop_loop_item_title' ); ?>
                                    </div>
                                    <div class="shop-product-single-item-content col-md-12 col-sm-12 col-xs-12">
                                        <?php
                                        /**
                * Aqui Esta el titulo
                * woocommerce_after_shop_loop_item_title hook
                *
                * @hooked woocommerce_template_loop_rating - 5
                * @hooked woocommerce_template_loop_price - 10
                */
                                        do_action( 'woocommerce_after_shop_loop_item_title' );
                                        ?>
                                        <a href="<?php echo home_url('/?add-to-wishlist=' . get_the_ID()); ?>"><span class="shop-item-sprite shop-item-sprite-cart-wish tienda-sprite tienda-sprite-cart-wish"></span></a>
                                        <a href="<?php echo home_url('/?add-to-cart=' . get_the_ID()); ?>"><span class="shop-item-sprite shop-item-sprite-cart-cart tienda-sprite tienda-sprite-cart-cart"></span></a>
                                        <a href="<?php the_permalink(); ?>"><span class="shop-item-sprite shop-item-sprite-cart-single tienda-sprite tienda-sprite-cart-single"></span></a>
                                    </div>

                                    <?php

                                    /**
         * woocommerce_after_shop_loop_item hook
         *
         * @hooked woocommerce_template_loop_add_to_cart - 10
         */
                                    do_action( 'woocommerce_after_shop_loop_item' );

                                    ?>
                                </div>
                            </div>
                            <?php if ($i == 8) { echo '</div>'; } ?>
                            <?php  $i++; if ($i > 8) { $i = 1; } endwhile; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
                <div class="row visible-sm visible-xs">
                    <div class="col-md-12 col-sm-12 col-xs-12  no-paddingl no-paddingr">
                        <h2 class="front-featured-prods-title">PRODUCTOS DESTACADOS</h2>

                        <?php $args = array ('post_type' => 'product', 'posts_per_page' => 12, 'tax_query' => array( array( 'taxonomy'  => 'product_cat', 'field' => 'slug', 'terms' => 'arma-tu-cesta', 'operator'  => 'NOT IN'))); ?>
                        <?php query_posts($args); $i=1; ?>
                        <?php while (have_posts()): the_post(); ?>
                        <?php if ($i == 1) { echo '<div class="gallery-cell col-md-12 col-sm-12 col-xs-12">'; } ?>
                        <div class="col-md-3 col-sm-6 col-xs-6 <?php echo join(' ', get_post_class()); ?>">
                            <div class="shop-product-single-item col-md-12 col-sm-12 col-xs-12  no-paddingl no-paddingr">
                                <?php do_action( 'woocommerce_before_shop_loop_item' ); ?>

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php
                                    /**
                 * woocommerce_before_shop_loop_item_title hook
                 *
                 * @hooked woocommerce_show_product_loop_sale_flash - 10
                 * @hooked woocommerce_template_loop_product_thumbnail - 10
                 */
                                    do_action( 'woocommerce_before_shop_loop_item_title' ); ?>
                                </div>
                                <div class="shop-product-single-item-price col-md-12 col-sm-12 col-xs-12">
                                    <?php
                                    /**
                * Aqui Esta el Price
                * woocommerce_shop_loop_item_title hook
                *
                * @hooked woocommerce_template_loop_product_title - 10
                */
                                    do_action( 'woocommerce_shop_loop_item_title' ); ?>
                                </div>
                                <div class="shop-product-single-item-content col-md-12 col-sm-12 col-xs-12">
                                    <?php
                                    /**
                * Aqui Esta el titulo
                * woocommerce_after_shop_loop_item_title hook
                *
                * @hooked woocommerce_template_loop_rating - 5
                * @hooked woocommerce_template_loop_price - 10
                */
                                    do_action( 'woocommerce_after_shop_loop_item_title' );
                                    ?>
                                    <span class="shop-item-sprite shop-item-sprite-cart-wish tienda-sprite tienda-sprite-cart-wish"></span>
                                    <a href="<?php echo home_url('/?add-to-cart=' . get_the_ID()); ?>"><span class="shop-item-sprite shop-item-sprite-cart-cart tienda-sprite tienda-sprite-cart-cart"></span></a>
                                    <a href="<?php the_permalink(); ?>"><span class="shop-item-sprite shop-item-sprite-cart-single tienda-sprite tienda-sprite-cart-single"></span></a>
                                </div>

                                <?php

                                /**
         * woocommerce_after_shop_loop_item hook
         *
         * @hooked woocommerce_template_loop_add_to_cart - 10
         */
                                do_action( 'woocommerce_after_shop_loop_item' );

                                ?>
                            </div>
                        </div>
                        <?php if ($i == 8) { echo '</div>'; } ?>
                        <?php  $i++; if ($i > 8) { $i = 1; } endwhile; ?>
                        <?php wp_reset_query(); ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="front-contact col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="front-contact-mask"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-8 col-xs-12">
                        <h2>SUGERENCIAS</h2>
                        <?php $contactobox = get_post_meta(get_the_ID(), 'rw_contactbox', true); ?>
                        <?php echo do_shortcode($contactobox); ?>
                    </div>
                    <div class="front-contact-reg col-md-3 col-md-offset-2 col-sm-4 hidden-xs">
                        <a href="<?php echo home_url('/mi-cuenta'); ?>"><button class="bt btn-lg btn-contact-reg">REGÍSTRATE</button></a>
                        <h2>SÍGUENOS</h2>
                        <a href="https://www.facebook.com/Lluviadeobsequios/?fref=ts" target="_blank"><i class="fa fa-facebook fa-3x"></i></a>
                        <a href="https://twitter.com/deobsequios" target="_blank"><i class="fa fa-twitter fa-3x"></i></a>
                        <a href="https://www.instagram.com/lluviadeobsequios/" target="_blank"><i class="fa fa-instagram fa-3x"></i></a>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>




