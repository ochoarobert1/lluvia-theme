<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main">
    <div class="row">
        <section class="page-content col-md-12 no-paddingl no-paddingr">
            <div class="faq-section-title col-md-12">
                <div class="container">
                    <div class="row">
                        <h1><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="container">
                <div class="row">
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <div class="col-md-10">
                            <?php the_content(); ?>
                        </div>
                    </article>
                </div>
            </div>
            <div class="prov-formulario col-md-12 no-paddingl no-paddingr">
                <div class="prov-formulario-mask"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-10 no-paddingl">
                            <?php $contact = get_post_meta(get_the_ID(), 'rw_prov_cf7', true); ?>
                            <?php echo do_shortcode($contact); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
