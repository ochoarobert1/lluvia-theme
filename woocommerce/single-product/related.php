<?php
/**
 * Related Products
 *
 * @author         WooThemes
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

if ( empty( $product ) || ! $product->exists() ) {
    return;
}

$cats_array = array(0);
// Get product categories
$terms = wp_get_post_terms( $product->id, 'product_cat' );

//Select only the category which doesn't have any children
if( sizeof( $terms ) ){
    foreach ( $terms as $term ) {
        $children = get_term_children( $term->term_id, 'product_cat' );
        if ( !sizeof( $children ) )
            $cats_array[] = $term->term_id;
    }
}


$relatedprods = get_post_meta(get_the_ID(), 'rw_check_related', true);
if (($relatedprods == 0) || ($relatedprods == '')) {
    $args = apply_filters( 'woocommerce_related_products_args', array(
        'post_type' => 'product',
        'ignore_sticky_posts' => 1,
        'no_found_rows' => 1,
        'posts_per_page' => $posts_per_page,
        'orderby' => $orderby,
        'post__not_in' => array( $product->id ),
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'id',
                'terms' => $cats_array
            ),
        )
    ) );

} else {
    $relatedprods = get_post_meta(get_the_ID(), 'rw_cats_related', true);
    $args = apply_filters( 'woocommerce_related_products_args', array(
        'post_type' => 'product',
        'ignore_sticky_posts' => 1,
        'no_found_rows' => 1,
        'posts_per_page' => $posts_per_page,
        'orderby' => $orderby,
        'post__not_in' => array( $product->id ),
        'tax_query' => array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'slug',
                'terms' => $relatedprods
            ),
        )
    ) );
}



$tipo = get_post_meta(get_the_ID(), '_bto_data', true);
if ($tipo == '') {

    $products = new WP_Query( $args );

    $woocommerce_loop['columns'] = $columns;

    if ( $products->have_posts() ) : ?>

<div class="related products">

    <h2><?php _e( 'Related Products', 'woocommerce' ); ?></h2>

    <?php woocommerce_product_loop_start(); ?>
    <div class="related-products">
        <?php while ( $products->have_posts() ) : $products->the_post(); ?>

        <?php wc_get_template_part( 'content', 'product' ); ?>

        <?php endwhile; // end of the loop. ?>
    </div>
    <?php woocommerce_product_loop_end(); ?>
</div>

<?php endif;

    wp_reset_postdata();

} ?>
