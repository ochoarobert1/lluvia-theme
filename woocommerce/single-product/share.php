<?php
/**
 * Single Product Share
 *
 * Sharing plugins can hook into here or you can add your own code directly.
 *
 * @author         WooThemes
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

?>
<div id="special-share" class="hide-share share-container col-md-12 animated fadeIn">
<?php do_action( 'woocommerce_share' ); // Sharing plugins can hook into here ?>
</div>
<div class="clearfix"></div>
