<?php
/**
 * Composited Variable Product Template.
 *
 * Override this template by copying it to 'yourtheme/woocommerce/composited-product/variable-product.php'.
 *
 * @version  3.2.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?><div class="details component_data" data-component_set="" data-price="0" data-regular_price="0" data-product_type="variable" data-product_variations="<?php echo esc_attr( json_encode( $product_variations ) ); ?>" data-custom="<?php echo esc_attr( json_encode( $custom_data ) ); ?>"><?php

	/**
	 * Composited product details template
	 *
	 * @hooked wc_cp_composited_product_excerpt - 10
	 */
	do_action( 'woocommerce_composited_product_details', $product, $component_id, $composite_product );

	?><table class="variations" cellspacing="0">
		<tbody><?php

			foreach ( $attributes as $attribute_name => $options ) {

				?><tr class="attribute-options" data-attribute_label="<?php echo wc_attribute_label( $attribute_name ); ?>">
					<td class="label">
						<label for="<?php echo sanitize_title( $attribute_name ); ?>"><?php echo wc_attribute_label( $attribute_name ); ?> <abbr class="required" title="required">*</abbr></label>
					</td>
					<td class="value"><?php
						$selected = isset( $_REQUEST[ 'wccp_attribute_' . sanitize_title( $attribute_name ) ][ $component_id ] ) ? wc_clean( $_REQUEST[ 'wccp_attribute_' . sanitize_title( $attribute_name ) ][ $component_id ] ) : wc_composite_get_variation_default_attribute( $product, $attribute_name );
						wc_composite_dropdown_variation_attribute_options( array( 'options' => $options, 'attribute' => $attribute_name, 'product' => $product, 'selected' => $selected ) );
						echo end( $attribute_keys ) === $attribute_name ? '<a class="reset_variations" href="#">' . __( 'Cancelar', 'woocommerce' ) . '</a>' : '';
					?></td>
				</tr><?php
			}

		?></tbody>
	</table><?php

	do_action( 'woocommerce_composited_product_add_to_cart', $product, $component_id, $composite_product );

	?><div class="single_variation_wrap component_wrap" style="display:none;">

		<div class="single_variation"></div>
		<div class="variations_button">
			<input type="hidden" name="variation_id" value="" /><?php

		 		wc_get_template( 'composited-product/quantity.php', array(
					'quantity_min'      => $quantity_min,
					'quantity_max'      => $quantity_max,
					'component_id'      => $component_id,
					'product'           => $product,
					'composite_product' => $composite_product
				), '', WC_CP()->plugin_path() . '/templates/' );

		 ?></div>
	</div>
</div>
