<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author         WooThemes
 * @package     WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
<section class="pre-categories col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <?php
                $taxonomies = array( 'product_cat' );
                $args = array('menu_order' => 'asc', 'hide_empty' => false, 'parent' => 0 );  ?>
                <?php $terms = get_terms( $taxonomies, $args ); ?>
                <?php if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){ ?>
                <?php foreach ( $terms as $term ) { ?>
                <?php if ($term->name == "Corporativo") { $term_link = home_url('/corporativo'); } else if ($term->slug == "arma-tu-cesta") { $term_link = home_url('/tienda/arma-tu-cesta'); } else { $term_link = get_term_link($term); }?> 
                <a href="<?php echo esc_url( $term_link ); ?>">
                    <div class="pre-category-item">
                        <?php $thumbnail_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true ); ?>
                        <?php $image = wp_get_attachment_url( $thumbnail_id ); ?>
                        <?php if ( $image ) { echo '<img src="' . $image . '" alt="" class="img-responsive"/>'; } ?>
                    </div>
                </a>
                <?php } ?>
                <?php } ?>
            </div>
            <div class="pre-category-item-title col-md-12 col-sm-12 col-xs-12">
                <?php if (is_shop()) { } else { ?>
                <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
                <h2 class="page-title"><?php woocommerce_page_title(); ?></h2>
                <?php endif; ?>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<section class="woocommerce-main-container">
    <div class="container">
        <div class="row">
            <aside class="col-md-3 col-sm-3 col-xs-3">
                <?php do_action( 'woocommerce_sidebar' ); ?>
            </aside>
            <div class="woocommerce-product-container col-md-9 col-sm-9 col-xs-9">
                <?php do_action( 'woocommerce_before_main_content' ); ?>
                <?php do_action( 'woocommerce_archive_description' ); ?>

                <?php if ( have_posts() ) : ?>

                <?php do_action( 'woocommerce_before_shop_loop' ); ?>
                <?php woocommerce_product_loop_start(); ?>
                <?php woocommerce_product_subcategories(); ?>
                <?php while ( have_posts() ) : the_post(); ?>
                <?php wc_get_template_part( 'content', 'product' ); ?>
                <?php endwhile; // end of the loop. ?>
                <?php woocommerce_product_loop_end(); ?>
                <?php do_action( 'woocommerce_after_shop_loop' ); ?>
                <?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
                <?PHP /* IF NOT */ ?>

                <?php global $wp_query; $search_query = $wp_query->query_vars['s']; ?>
                <?php $mypostids = multiplevars($search_query); ?>
                <?php 
                $args = array(
                    'post__in'=> $mypostids,
                    'post_type'=>'product',
                    'orderby'=>'title',
                    'order'=>'asc', 'tax_query', array(array(
                        'taxonomy' => 'product_cat',
                        'field' => 'slug',
                        'terms' => array( 'corporativo', 'arma-tu-cesta' ), // Don't display products in the knives category on the shop page
                        'operator' => 'NOT IN'
                    )));
                ?>
                <?php query_posts($args) ?>
                <?php if ( have_posts() ) { ?>
                <?php woocommerce_product_loop_start(); ?>
                <?php while ( have_posts() ) : the_post(); ?>
                <?php wc_get_template_part( 'content', 'product' ); ?>
                <?php endwhile; // end of the loop. ?>
                <?php woocommerce_product_loop_end(); ?>
                <?php } else { ?>
                <?php wc_get_template( 'loop/no-products-found.php' ); ?>
                <?php } ?>
                <?php endif; ?>

                <?php do_action( 'woocommerce_after_main_content' ); ?>

            </div>
        </div>
    </div>
</section>

<?php get_footer( 'shop' ); ?>
