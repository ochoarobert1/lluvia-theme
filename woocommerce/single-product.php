<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author         WooThemes
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
<?php $defaultatts = array ('class' => 'img-responsive'); ?>
<section class="pre-categories col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
    <div class="container">
        <div class="row">
            <?php $tipo = get_post_meta(get_the_ID(), '_bto_data', true); ?>
            <?php if ($tipo == '') { ?>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <?php
    $taxonomies = array( 'product_cat' );
    $args = array('menu_order' => 'asc', 'hide_empty' => false, 'parent' => 0 );  ?>
                <?php $terms = get_terms( $taxonomies, $args ); ?>
                <?php if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){ ?>
                <?php foreach ( $terms as $term ) { ?>
                <?php if ($term->name == "Corporativo") { $term_link = home_url('/corporativo'); } else if ($term->slug == "arma-tu-cesta") { $term_link = home_url('/tienda/arma-tu-cesta'); } else { $term_link = get_term_link($term); }?>
                <a href="<?php echo esc_url( $term_link ); ?>">
                    <div class="pre-category-item">
                        <?php $thumbnail_id = get_woocommerce_term_meta( $term->term_id, 'thumbnail_id', true ); ?>
                        <?php $image = wp_get_attachment_url( $thumbnail_id ); ?>
                        <?php if ( $image ) { echo '<img src="' . $image . '" alt="" class="img-responsive"/>'; } ?>
                    </div>
                </a>
                <?php } ?>
                <?php } ?>
            </div>
            <?php } else { ?>
            <div class="pre-categories-cesta col-md-12 col-sm-12 col-xs-12">
                <?php $args = array ('post_type' => 'product', 'posts_per_page' => -1, 'tax_query' => array ( array ('taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => 'arma-tu-cesta', 'include_children' => false))); ?>
                <?php query_posts($args)?>
                <?php while (have_posts()) : the_post(); ?>
                <?php $terms = get_the_terms(get_the_ID(), 'product_cat'); ?>
                <?php $max = sizeof($terms); ?>
                <?php if ($max <= 1) { ?>
                <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                    <?php the_post_thumbnail('avatar',$defaultatts); ?>
                </a>
                <?php } ?>
                <?php endwhile; wp_reset_query();?>
                <?php } ?>
            </div>

        </div>
    </div>
    <?php $bar_color = get_post_meta(get_the_ID(), 'rw_bar_color', true); ?>
    <?php $text_color = get_post_meta(get_the_ID(), 'rw_text_color', true); ?>
    <div class="pre-category-item-title col-md-12 col-sm-12 col-xs-12 <?php if ($tipo != '') { echo 'pre-category-cesta-title'; } ?>" style="background-color: <?php echo $bar_color; ?> !important;">
        <?php if ($tipo == '') { ?>
        <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
        <h2 class="page-title" style="color: <?php echo $text_color; ?> !important;"><?php woocommerce_page_title(); ?></h2>
        <?php endif; ?>
        <?php } else { echo '<h2 class="page-title">' . get_the_title() . '</h2>'; } ?>
    </div>
</section>
<section class="woocommerce-main-container">
    <div class="container">
        <div class="row">
            <?php if ($tipo == '') { ?>
            <aside class="col-md-3 col-sm-3 col-xs-3">
                <?php /**
                        * woocommerce_sidebar hook
                        *
                        * @hooked woocommerce_get_sidebar - 10
                        */
    do_action( 'woocommerce_sidebar' );
                ?>
            </aside>
            <?php } ?>




            <div class="custom-single-product-container <?php if ($tipo == '') { echo 'col-md-9 col-sm-9 col-xs-9'; } else { echo 'col-md-12 col-sm-12 col-xs-12'; } ?>">
                <?php
                /**
         * woocommerce_before_main_content hook
         *
         * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
         * @hooked woocommerce_breadcrumb - 20
         */
                do_action( 'woocommerce_before_main_content' );
                ?>
                <?php while ( have_posts() ) : the_post(); ?>
                <?php wc_get_template_part( 'content', 'single-product' ); ?>
                <?php endwhile; // end of the loop. ?>
                <?php
                /**
         * woocommerce_after_main_content hook
         *
         * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
         */
                do_action( 'woocommerce_after_main_content' );
                ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer( 'shop' ); ?>
