<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main">
    <div class="row">
        <div class="faq-section-title col-md-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <section class="col-md-12 page-content">
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <?php the_content(); ?>
                        <?php comments_template( '', true ); // Remove if you don't want comments ?>
                        <br class="clear">
                        <?php edit_post_link(); ?>
                    </article>
                </section>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
