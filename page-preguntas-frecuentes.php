<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main">
    <div class="row">
        <section class="page-content col-md-12 no-paddingl no-paddingr">
            <div class="faq-section-title col-md-12">
                <div class="container">
                    <div class="row">
                        <h1><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <div class="faq-content col-md-12">
                            <?php the_content(); ?>
                            <div class="clearfix"></div>
                            <?php $taxonomies = array('tax_preguntas');
                            $args = array('orderby' => 'id', 'order' => 'ASC');
                            $terms = get_terms($taxonomies, $args);
                            if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){ ?>
                            <?php foreach ( $terms as $term ) { ?>
                            <h3 class="faq-title"><?php echo $term->name; ?></h3>
                            <div class="col-md-10">
                                <div class="panel-group" id="<?php echo $term->slug; ?>" role="tablist" aria-multiselectable="true">
                                    <?php $args = array('post_type' => 'preguntas', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'name', 'tax_query' => array( array ( 'taxonomy' => 'tax_preguntas', 'field' => 'slug', 'terms' => $term->slug )));?>
                                    <?php query_posts($args); $i = 1; ?>
                                    <?php while (have_posts()) : the_post() ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="h-<?php echo $term->slug . '-' .$i; ?>">
                                            <h4 class="panel-title" role="button" data-toggle="collapse" data-parent="#<?php echo $term->slug; ?>" href="#c-<?php echo $term->slug . '-' .$i; ?>" aria-expanded="true" aria-controls="c-<?php echo $term->slug . '-' .$i; ?>">
                                                <a><?php the_title(); ?></a><span class="glyphicon glyphicon-chevron-up"></span>
                                            </h4>
                                        </div>
                                        <div id="c-<?php echo $term->slug . '-' .$i; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="h-<?php echo $term->slug . '-' .$i; ?>">
                                            <div class="panel-body">
                                                <?php the_content(); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $i++; endwhile; ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <?php } ?>
                            <?php } ?>
                        </div>

                    </article>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
