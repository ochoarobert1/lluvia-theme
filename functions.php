<?php

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER CSS
-------------------------------------------------------------- */

function lluvia_load_css() {
    if (!is_admin()){
        $version_remove = NULL;
        if ($_SERVER['REMOTE_ADDR'] == '::1') {

            /*- BOOTSTRAP CORE ON LOCAL -*/
            wp_register_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', false, '3.3.5', 'all');
            wp_enqueue_style('bootstrap-css');

            /*- BOOTSTRAP THEME ON LOCAL -*/
            wp_register_style('bootstrap-theme', get_template_directory_uri() . '/css/bootstrap-theme.min.css', array('bootstrap-css'), '3.3.5', 'all');
            wp_enqueue_style('bootstrap-theme');

            /*- FONT AWESOME ON LOCAL -*/
            wp_register_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', false, '4.4.0', 'all');
            wp_enqueue_style('font-awesome');

            /*- VALIDATION ENGINE ON LOCAL -*/
            wp_register_style('validation-engine', get_template_directory_uri() . '/css/validationEngine.min.css', false, '2.6.4', 'all');
            wp_enqueue_style('validation-engine');

            /*- ANIMATE CSS ON LOCAL -*/
            wp_register_style('animate-css', get_template_directory_uri() . '/css/animate.css', false, '3.4.0', 'all');
            wp_enqueue_style('animate-css');

            /*- FLICKITY ON LOCAL -*/
            wp_register_style('flickity-css', get_template_directory_uri() . '/css/flickity.min.css', false, '1.1.0', 'all');
            wp_enqueue_style('flickity-css');

            /*- JQUERY UI CSS  -*/
            wp_register_style('jquery-ui-css', get_template_directory_uri() . '/css/jquery-ui.min.css', false, '1.11.4', 'all');
            wp_enqueue_style('jquery-ui-css');

        } else {

            /*- BOOTSTRAP CORE -*/
            wp_register_style('bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css', false, '3.3.5', 'all');
            wp_enqueue_style('bootstrap-css');

            /*- BOOTSTRAP THEME -*/
            wp_register_style('bootstrap-theme', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css', array('bootstrap-css'), '3.3.5', 'all');
            wp_enqueue_style('bootstrap-theme');

            /*- FONT AWESOME -*/
            wp_register_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', false, '4.4.0', 'all');
            wp_enqueue_style('font-awesome');

            /*- VALIDATION ENGINE -*/
            wp_register_style('validation-engine', 'https://cdn.jsdelivr.net/jquery.validationengine/2.6.4/css/validationEngine.jquery.css', false, '2.6.4', 'all');
            wp_enqueue_style('validation-engine');

            /*- ANIMATE CSS -*/
            wp_register_style('animate-css', 'https://cdn.jsdelivr.net/animatecss/3.4.0/animate.min.css', false, '3.4.0', 'all');
            wp_enqueue_style('animate-css');

            /*- FLICKITY -*/
            wp_register_style('flickity-css', 'https://cdn.jsdelivr.net/flickity/1.1.0/flickity.min.css', false, '1.1.0', 'all');
            wp_enqueue_style('flickity-css');

            /*- JQUERY UI CSS  -*/
            wp_register_style('jquery-ui-css', 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.css', false, '1.11.4', 'all');
            wp_enqueue_style('jquery-ui-css');

        }

        /*- OWL CAROUSEL -*/
        wp_register_style('owl-css', get_template_directory_uri() . '/css/owl.carousel.css', false, $version_remove, 'all');
        wp_enqueue_style('owl-css');

        /*- OWL THEME -*/
        wp_register_style('owl-theme', get_template_directory_uri() . '/css/owl.theme.css', false, $version_remove, 'all');
        wp_enqueue_style('owl-theme');

        /*- OWL THEME -*/
        wp_register_style('owl-transitions', get_template_directory_uri() . '/css/owl.transitions.css', false, $version_remove, 'all');
        wp_enqueue_style('owl-transitions');

        /*- GOOGLE FONTS -*/
        wp_register_style('google-fonts', 'https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900', false, $version_remove, 'all');
        wp_enqueue_style('google-fonts');

        /*- JQUERY UI CSS  -*/
        wp_register_style('jquery-ui-theme-css',  get_template_directory_uri() . '/css/jquery-ui.theme.min.css', false, '1.11.4', 'all');
        wp_enqueue_style('jquery-ui-theme-css');

        /*- MAIN STYLE -*/
        wp_register_style('main-style', get_template_directory_uri() . '/css/lluvia-style.css', false, $version_remove, 'all');
        wp_enqueue_style('main-style');

        /*- MAIN MEDIAQUERIES -*/
        wp_register_style('main-mediaqueries', get_template_directory_uri() . '/css/lluvia-mediaqueries.css', array('main-style'), $version_remove, 'all');
        wp_enqueue_style('main-mediaqueries');
    }
}

add_action('init', 'lluvia_load_css');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER JS
-------------------------------------------------------------- */

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", '');
function my_jquery_enqueue() {
    wp_deregister_script('jquery');
    if ($_SERVER['REMOTE_ADDR'] == '::1') {
        /*- JQUERY ON LOCAL  -*/
        wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '1.11.3', true);
    } else {
        /*- JQUERY ON WEB  -*/
        wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js', false, '1.11.3', true);
    }
    wp_enqueue_script('jquery');
}


function lluvia_load_js() {
    if (!is_admin()){
        $version_remove = NULL;
        if ($_SERVER['REMOTE_ADDR'] == '::1') {
            /*- MODERNIZR ON LOCAL  -*/
            wp_register_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.min.js', array('jquery'), '2.8.3', true);
            wp_enqueue_script('modernizr');

            /*- BOOTSTRAP ON LOCAL  -*/
            wp_register_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.5', true);
            wp_enqueue_script('bootstrap');

            /*- JQUERY STICKY ON LOCAL  -*/
            wp_register_script( 'sticky', get_template_directory_uri() . '/js/jquery.sticky.js', array('jquery'), '1.0.1', true);
            wp_enqueue_script('sticky');

            /*- VALIDATION ENGINE ON LOCAL  -*/
            wp_register_script( 'validation', get_template_directory_uri() . '/js/jquery.validationEngine.min.js', array('jquery'), '2.6.4', true);
            wp_enqueue_script('validation');

            /*- JQUERY NICESCROLL ON LOCAL  -*/
            wp_register_script( 'nicescroll', get_template_directory_uri() . '/js/jquery.nicescroll.min.js', array('jquery'), '3.6.0', true);
            wp_enqueue_script('nicescroll');

            /*- LETTERING  -*/
            wp_register_script( 'lettering', get_template_directory_uri() . '/js/jquery.lettering.js', array('jquery'), '0.7.0', true);
            wp_enqueue_script('lettering');

            /*- IMAGESLOADED ON LOCAL  -*/
            //wp_register_script( 'imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.js', array('jquery'), '3.1.8', true);
            //wp_enqueue_script('imagesloaded');

            /*- SMOOTH SCROLL -*/
            wp_register_script( 'smooth-scroll', get_template_directory_uri() . '/js/smooth-scroll.js', array('jquery'), '10.0.0', true);
            wp_enqueue_script('smooth-scroll');

            /*- ISOTOPE ON LOCAL  -*/
            //wp_register_script( 'isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array('jquery'), '2.2.1', true);
            //wp_enqueue_script('isotope');

            /*- FLICKITY ON LOCAL  -*/
            wp_register_script( 'flickity', get_template_directory_uri() . '/js/flickity.pkgd.min.js', array('jquery'), '1.1.0', true);
            wp_enqueue_script('flickity');

            /*- MASONRY ON LOCAL  -*/
            //wp_register_script( 'masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array('jquery'), '3.3.1', true);
            //wp_enqueue_script('masonry');

            /* JQUERY UI */
            wp_register_script( 'jqueryui',  get_template_directory_uri() . '/js/jquery-ui.min.js', array('jquery'), '1.11.4', true);
            wp_enqueue_script('jqueryui');

        } else {


            /*- MODERNIZR -*/
            wp_register_script( 'modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array('jquery'), '2.8.3', true);
            wp_enqueue_script('modernizr');

            /*- BOOTSTRAP -*/
            wp_register_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js', array('jquery'), '3.3.5', true);
            wp_enqueue_script('bootstrap');

            /*- JQUERY STICKY -*/
            wp_register_script( 'sticky', 'https://cdn.jsdelivr.net/jquery.sticky/1.0.1/jquery.sticky.min.js', array('jquery'), '1.0.1', true);
            wp_enqueue_script('sticky');

            /*- VALIDATION ENGINE -*/
            wp_register_script( 'validation', 'https://cdnjs.cloudflare.com/ajax/libs/jQuery-Validation-Engine/2.6.4/jquery.validationEngine.min.js', array('jquery'), '2.6.4', true);
            wp_enqueue_script('validation');

            /*- JQUERY NICESCROLL -*/
            wp_register_script( 'nicescroll', 'https://cdn.jsdelivr.net/jquery.nicescroll/3.6.0/jquery.nicescroll.min.js', array('jquery'), '3.6.0', true);
            wp_enqueue_script('nicescroll');

            /*- LETTERING  -*/
            wp_register_script( 'lettering', 'https://cdnjs.cloudflare.com/ajax/libs/lettering.js/0.7.0/jquery.lettering.min.js', array('jquery'), '0.7.0', true);
            wp_enqueue_script('lettering');

            /*- SMOOTH SCROLL -*/
            wp_register_script( 'smooth-scroll', 'https://cdnjs.cloudflare.com/ajax/libs/smooth-scroll/10.0.0/js/smooth-scroll.min.js', array('jquery'), '10.0.0', true);
            wp_enqueue_script('smooth-scroll');

            /*- IMAGESLOADED -*/
            //wp_register_script( 'imagesloaded', 'https://cdn.jsdelivr.net/imagesloaded/3.1.8/imagesloaded.pkgd.js', array('jquery'), '3.1.8', true);
            //wp_enqueue_script('imagesloaded');


            /*- ISOTOPE -*/
            //wp_register_script( 'isotope', 'https://cdn.jsdelivr.net/isotope/2.2.1/isotope.pkgd.min.js', array('jquery'), '2.2.1', true);
            //wp_enqueue_script('isotope');

            /*- FLICKITY -*/
            wp_register_script( 'flickity', 'https://cdn.jsdelivr.net/flickity/1.1.0/flickity.pkgd.min.js', array('jquery'), '1.1.0', true);
            wp_enqueue_script('flickity');

            /*- MASONRY -*/
            //wp_register_script( 'masonry', 'https://cdn.jsdelivr.net/masonry/3.3.1/masonry.pkgd.min.js', array('jquery'), '3.3.1', true);
            //wp_enqueue_script('masonry');

            /* JQUERY UI */
            wp_register_script( 'jqueryui', 'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js', array('jquery'), '1.11.4', true);
            wp_enqueue_script('jqueryui');



        }

        /*- INPUTMASK -*/
        wp_register_script('inputmask-js', get_template_directory_uri() . '/js/jquery.inputmask.bundle.min.js', array('jquery'), $version_remove, true);
        wp_enqueue_script('inputmask-js');

        /*- DATEPCIKER ESPAÑOL -*/
        wp_register_script('datepicker-es-js', get_template_directory_uri() . '/js/datepicker-es.js', array('jquery'), $version_remove, true);
        wp_enqueue_script('datepicker-es-js');

        /*- owl JS -*/
        wp_register_script('owl-js', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), $version_remove, true);
        wp_enqueue_script('owl-js');

        /*- FLICKITY IMAGESLOADED -*/
        wp_register_script('fimages', get_template_directory_uri() . '/js/flickity-imagesloaded.js', array('jquery', 'flickity'), $version_remove, true);
        wp_enqueue_script('fimages');

        /*- VALIDATION ENGINE LOCALE -*/
        wp_register_script('validation-es', get_template_directory_uri() . '/js/jquery.validationEngine-es.min.js', array('jquery', 'validation'), $version_remove, true);
        wp_enqueue_script('validation-es');

        /*- WOW -*/
        wp_register_script('wow', get_template_directory_uri() . '/js/wow.min.js', array('jquery'), $version_remove, true);
        wp_enqueue_script('wow');

        /*- MAIN FUNCTIONS -*/
        wp_register_script('main-functions', get_template_directory_uri() . '/js/functions.js', array('jquery'), $version_remove, true);
        wp_enqueue_script('main-functions');
    }
}

add_action('init', 'lluvia_load_js');

/* --------------------------------------------------------------
    ADD THEME SUPPORT
-------------------------------------------------------------- */

load_theme_textdomain('lluvia', get_template_directory() . '/languages');
add_theme_support('post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio' ));
add_theme_support('post-thumbnails');
add_theme_support('automatic-feed-links');
add_theme_support('title-tag');
add_theme_support('menus');

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */
function lluvia_add_editor_styles() {
    add_editor_style( get_stylesheet_directory_uri() . '/css/editor-styles.css' );
}
add_action( 'admin_init', 'lluvia_add_editor_styles' );

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */

register_nav_menus( array(
    'header_menu' => __( 'Menu Header Principal', 'lluvia' ),
    'footer_menu' => __( 'Menu Footer', 'lluvia' ),
) );

/* --------------------------------------------------------------
    ADD DYNAMIC SIDEBAR SUPPORT
-------------------------------------------------------------- */

add_action( 'widgets_init', 'lluvia_widgets_init' );
function lluvia_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'lluvia' ),
        'id' => 'main_sidebar',
        'description' => __( 'Widgets seran vistos en posts y pages', 'lluvia' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
    register_sidebar( array(
        'name' => __( 'Shop Sidebar', 'lluvia' ),
        'id' => 'shop_sidebar',
        'description' => __( 'Estos elementos se veran en los sitios de productos/tienda', 'lluvia' ),
        'before_widget' => '<li id="%1$s" class="widget widget-custom-woocommerce %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}



/* --------------------------------------------------------------
    CUSTOM ADMIN LOGIN
-------------------------------------------------------------- */

add_action('login_head', 'custom_login_logo');
function custom_login_logo() {
    echo '
    <style type="text/css">
        body{
            background-color: #ECECEC !important;
            background-image:url(' . get_template_directory_uri() . '/images/login-bg.jpg);
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
        }
        h1 {
            background-image:url(' . get_template_directory_uri() . '/images/logo.png) !important;
            background-repeat: no-repeat;
            background-position: center;
            background-size: contain !important;
        }
        a { background-image:none !important;}
        .login form{-webkit-border-radius: 5px; border-radius: 5px; background-color: rgba(255,255,255,0.5);}
        .login label{color: black; font-weight: 500;}
    </style>
    ';
}

if (! function_exists('dashboard_footer') ){
    function dashboard_footer() {
        _e( '<span id="footer-thankyou">Gracias por crear con <a href="http://wordpress.org/" >WordPress.</a> - Tema desarrollado por <a href="http://dlkestudio.com/" >dlk estudio</a></span>', 'lluvia' );
    }
}
add_filter('admin_footer_text', 'dashboard_footer');


/* --------------------------------------------------------------
    PRODUCT LIST
-------------------------------------------------------------- */

function prodList() {
    global $wpdb;
    $the_query = "SELECT wp_terms.term_id, wp_terms.name, wp_terms.slug
FROM wp_term_taxonomy
LEFT JOIN wp_terms
ON wp_term_taxonomy.term_id = wp_terms.term_id
WHERE taxonomy LIKE 'product_cat' AND parent = 0 ORDER BY wp_terms.term_id";
    $posts_year_range = $wpdb->get_results($the_query, 'ARRAY_A');
    $item_container = [];
    $i = 1;

    foreach ($posts_year_range as $item){
        if ($item['slug'] != 'arma-tu-cesta') {
            $itemkeys[] = $item['slug'];
            $itemvalues[] = $item['name'];
            $thequery2 = "SELECT wp_terms.term_id, wp_terms.name, wp_terms.slug FROM wp_term_taxonomy LEFT JOIN wp_terms ON wp_term_taxonomy.term_id = wp_terms.term_id WHERE taxonomy LIKE 'product_cat' AND parent like " . $item['term_id'];
            $childrens = $wpdb->get_results($thequery2, 'ARRAY_A');
            if ($childrens != NULL){
                foreach ($childrens as $item2){
                    $itemkeys[] = $item2['slug'];
                    $itemvalues[] = '-- ' . $item2['name'];
                }
            }
        }
    }
    $item_container[] = array_combine($itemkeys, $itemvalues);

    return $item_container[0];
}




/* --------------------------------------------------------------
    ADD CUSTOM METABOX
-------------------------------------------------------------- */

add_filter( 'rwmb_meta_boxes', 'lluvia_metabox' );

function lluvia_metabox( $meta_boxes )
{
    $prefix = 'rw_';

    $meta_boxes[] = array(
        'title'    => 'Pagina Principal - Personalizado',
        'pages'    => array( 'page' ),
        'include' => array(
            'relation' => 'OR',
            /*'ID'         => array(9),*/
            'slug'       => array( 'inicio' )
        ),
        'fields' => array(
            array(
                'name' => 'Custom Contact Box',
                'desc' => 'Aqui agregamos el formulario de contacto via Custom forms (CF7)',
                'id'   => $prefix . 'contactbox',
                'type' => 'textarea',
            ),
        )
    );

    $meta_boxes[] = array(
        'title'    => 'Promociones Atributos',
        'pages'    => array( 'promociones' ),
        'fields' => array(
            array(
                'id'     => 'promo_group',
                'type'   => 'group',
                'clone'  => true,
                'sort_clone' => true,
                'fields' => array(
                    array(
                        'name' => __( 'Imagen de Promo', 'lluvia' ),
                        'id'   => 'banner_promo',
                        'desc' => 'Tamaños: <br/> 1.- Pequeñas: 265px x 390px <br/> 2.- Grandes: 555px x 390px <br/> 3.- Completa: 1140px x 390px',
                        'type' => 'file_input',
                    ),
                    array(
                        'name' => __( 'Link del Promo', 'lluvia' ),
                        'id'   => 'link_promo',
                        'desc' => 'Enlace a donde debe ir la promo en cuestión',
                        'type' => 'text',
                    ),
                ),
            ),
        ),
    );

    $meta_boxes[] = array(
        'title'    => 'Contact Form para Proveedores',
        'pages'    => array( 'page' ),
        'include' => array(
            'relation' => 'OR',
            /*'ID'         => array(9),*/
            'slug'       => array( 'proveedores' )
        ),
        'fields' => array(
            array(
                'name' => 'Contact Box',
                'desc' => 'Aqui agregamos el formulario de contacto via Custom forms (CF7)',
                'id'   => $prefix . 'prov_cf7',
                'type' => 'text',
            ),
        )
    );

    $meta_boxes[] = array(
        'title'    => 'Relacionados:',
        'pages'    => array( 'product' ),
        'fields' => array(
            array(
                'name' => 'Activar Relacionados por Categoría',
                'id'   => $prefix . 'check_related',
                'type' => 'checkbox'
            ),
            array(
                'name' => 'Buscar',
                'id'   => $prefix . 'cats_related',
                'type' => 'select',
                'options'     => prodList(),
                // Select multiple values, optional. Default is false.
                'multiple'    => false,
                // Placeholder
                'placeholder' => __( 'Selecciona una Categoría', 'lluvia' ),
            ),
        )
    );

    $meta_boxes[] = array(
        'title'    => 'Color Para Cestas de Regalo:',
        'pages'    => array( 'product' ),
        'context' => 'side',
        'fields' => array(
            array(
                'name' => 'Color para barra de titulo',
                'desc' => 'Este color se verá en la barra horizontal que contiene el título',
                'id'   => $prefix . 'bar_color',
                'type' => 'color'
            ),
            array(
                'name' => 'Color para Texto',
                'desc' => 'Este color sera para el texto',
                'id'   => $prefix . 'text_color',
                'type' => 'color'
            ),
        )
    );

    return $meta_boxes;
}

/* --------------------------------------------------------------
    ADD CUSTOM POST TYPE
-------------------------------------------------------------- */

// Register Custom Post Type
function promociones() {

    $labels = array(
        'name'                  => _x( 'Promociones', 'Post Type General Name', 'lluvia' ),
        'singular_name'         => _x( 'Promoción', 'Post Type Singular Name', 'lluvia' ),
        'menu_name'             => __( 'Promociones', 'lluvia' ),
        'name_admin_bar'        => __( 'Promociones', 'lluvia' ),
        'archives'              => __( 'Archivo', 'lluvia' ),
        'parent_item_colon'     => __( 'Padre:', 'lluvia' ),
        'all_items'             => __( 'Todos', 'lluvia' ),
        'add_new_item'          => __( 'Agregar Nuevo', 'lluvia' ),
        'add_new'               => __( 'Agregar Nuevo', 'lluvia' ),
        'new_item'              => __( 'Nuevo', 'lluvia' ),
        'edit_item'             => __( 'Editar', 'lluvia' ),
        'update_item'           => __( 'Actualizar', 'lluvia' ),
        'view_item'             => __( 'Ver', 'lluvia' ),
        'search_items'          => __( 'Buscar', 'lluvia' ),
        'not_found'             => __( 'No hay resultado', 'lluvia' ),
        'not_found_in_trash'    => __( 'No hay resultados en la papelera', 'lluvia' ),
        'featured_image'        => __( 'Imagen destacada', 'lluvia' ),
        'set_featured_image'    => __( 'Configurar imagen destacada', 'lluvia' ),
        'remove_featured_image' => __( 'Remover imagen destacada', 'lluvia' ),
        'use_featured_image'    => __( 'Usar como imagen destacada', 'lluvia' ),
        'insert_into_item'      => __( 'Insertar en Item', 'lluvia' ),
        'uploaded_to_this_item' => __( 'Cargar a este item', 'lluvia' ),
        'items_list'            => __( 'Lista', 'lluvia' ),
        'items_list_navigation' => __( 'Navegación', 'lluvia' ),
        'filter_items_list'     => __( 'Filtro', 'lluvia' ),
    );
    $args = array(
        'label'                 => __( 'Promoción', 'lluvia' ),
        'description'           => __( 'Promociones del Home', 'lluvia' ),
        'labels'                => $labels,
        'supports'              => array( 'title', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-tickets-alt',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'promociones', $args );

}
add_action( 'init', 'promociones', 0 );

// Register Custom Post Type
function custom_faqs() {

    $labels = array(
        'name'                  => _x( 'Preguntas', 'Post Type General Name', 'lluvia' ),
        'singular_name'         => _x( 'Pregunta', 'Post Type Singular Name', 'lluvia' ),
        'menu_name'             => __( 'Preguntas Freq.', 'lluvia' ),
        'name_admin_bar'        => __( 'Preguntas Freq.', 'lluvia' ),
        'archives'              => __( 'Preguntas Frecuentes', 'lluvia' ),
        'parent_item_colon'     => __( 'Pregunta Padre:', 'lluvia' ),
        'all_items'             => __( 'Todas las Preguntas', 'lluvia' ),
        'add_new_item'          => __( 'Agregar', 'lluvia' ),
        'add_new'               => __( 'Agregar', 'lluvia' ),
        'new_item'              => __( 'Nuevo', 'lluvia' ),
        'edit_item'             => __( 'Editar', 'lluvia' ),
        'update_item'           => __( 'Actualizar', 'lluvia' ),
        'view_item'             => __( 'Ver', 'lluvia' ),
        'search_items'          => __( 'Buscar', 'lluvia' ),
        'not_found'             => __( 'No hay resultados', 'lluvia' ),
        'not_found_in_trash'    => __( 'No hay resultados en la Papelera', 'lluvia' ),
        'featured_image'        => __( 'Imagen Destacada', 'lluvia' ),
        'set_featured_image'    => __( 'Colocar imagen Destacada', 'lluvia' ),
        'remove_featured_image' => __( 'Remover imagen Destacada', 'lluvia' ),
        'use_featured_image'    => __( 'Usar Como imagen destacada', 'lluvia' ),
        'insert_into_item'      => __( 'Insertar en Pregunta', 'lluvia' ),
        'uploaded_to_this_item' => __( 'Cargada a esta pregunta', 'lluvia' ),
        'items_list'            => __( 'Lista de Preguntas', 'lluvia' ),
        'items_list_navigation' => __( 'Navegacion', 'lluvia' ),
        'filter_items_list'     => __( 'Filtro', 'lluvia' ),
    );
    $args = array(
        'label'                 => __( 'Pregunta', 'lluvia' ),
        'description'           => __( 'Preguntas Frecuentes del sitio', 'lluvia' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-editor-help',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'preguntas', $args );

}
add_action( 'init', 'custom_faqs', 0 );

// Register Custom Taxonomy
function custom_preguntas() {

    $labels = array(
        'name'                       => _x( 'Categorias', 'Taxonomy General Name', 'lluvia' ),
        'singular_name'              => _x( 'Categoría', 'Taxonomy Singular Name', 'lluvia' ),
        'menu_name'                  => __( 'Categorias', 'lluvia' ),
        'all_items'                  => __( 'Todas', 'lluvia' ),
        'parent_item'                => __( 'Padre', 'lluvia' ),
        'parent_item_colon'          => __( 'Item Padre', 'lluvia' ),
        'new_item_name'              => __( 'Nueva', 'lluvia' ),
        'add_new_item'               => __( 'Agregar', 'lluvia' ),
        'edit_item'                  => __( 'Editar', 'lluvia' ),
        'update_item'                => __( 'Actualizar', 'lluvia' ),
        'view_item'                  => __( 'Ver', 'lluvia' ),
        'separate_items_with_commas' => __( 'Separar por comas', 'lluvia' ),
        'add_or_remove_items'        => __( 'Agregar o Remover', 'lluvia' ),
        'choose_from_most_used'      => __( 'Escoger de las populares', 'lluvia' ),
        'popular_items'              => __( 'Items Populares', 'lluvia' ),
        'search_items'               => __( 'Buscar', 'lluvia' ),
        'not_found'                  => __( 'No hay resultados', 'lluvia' ),
        'no_terms'                   => __( 'No hay Items', 'lluvia' ),
        'items_list'                 => __( 'Lista', 'lluvia' ),
        'items_list_navigation'      => __( 'Navegación', 'lluvia' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'tax_preguntas', array( 'preguntas' ), $args );

}
add_action( 'init', 'custom_preguntas', 0 );

/* --------------------------------------------------------------
    ADD CUSTOM IMAGE SIZE
-------------------------------------------------------------- */
if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 9999, 400, true);
}
if ( function_exists( 'add_image_size' ) ) {
    add_image_size('avatar', 100, 100, true);
    add_image_size('blog_img', 276, 217, true);
    add_image_size( 'latest_blog', 310, 180, true );
    add_image_size( 'single_img', 636, 297, true );
    add_image_size( 'shop-item', 280, 200, true);
    add_image_size('shop_single_custom', 400, 400, false);
    add_image_size('shop_thumbnail_custom', 200, 200, false);
}


/* --------------------------------------------------------------
    ADD CUSTOM WALKER BOOTSTRAP
-------------------------------------------------------------- */

// WALKER COMPLETO TOMADO DESDE EL NAVBAR COLLAPSE
require_once('inc/wp_bootstrap_navwalker.php');

// WALKER CUSTOM SI DEBO COLOCAR ICONOS AL LADO DEL MENU PRINCIPAL - SU ESTRUCTURA ESTA DENTRO DEL MISMO ARCHIVO
/* require_once('inc/wp_walker_custom.php');*/

/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('inc/wp_custom_functions.php');

/* --------------------------------------------------------------
    ADD CUSTOM WOOCOMMERCE FUNCTIONS
-------------------------------------------------------------- */

require_once('inc/wp_custom_woocommerce.php');

/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS METABOX - EN DESARROLLO
-------------------------------------------------------------- */

/*- require_once('inc/wp_custom_metabox.php'); -*/


class themeslug_walker_nav_menu extends Walker_Nav_Menu {

    // add classes to ul sub-menus
    function start_lvl( &$output, $depth ) {
        // depth dependent classes
        $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
        $display_depth = ( $depth + 1); // because it counts the first submenu as 0
        $classes = array(
            'sub-menu',
            ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
            ( $display_depth >=2 ? 'sub-sub-menu' : '' ),
            'menu-depth-' . $display_depth
        );
        $class_names = implode( ' ', $classes );

        // build html
        $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
    }

    // add main/sub classes to li's and links
    function start_el( &$output, $item, $depth, $args ) {
        global $wp_query;
        $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

        // depth dependent classes
        $depth_classes = array(
            ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
            ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
            ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
            'menu-item-depth-' . $depth
        );
        $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

        // passed classes
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

        // build html
        $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';

        // link attributes
        $category = get_term_by('ID', $item->object_id, 'tax_preguntas');
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( home_url('/preguntas-frecuentes#'. $category->slug) ) .'"' : '';
        $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';

        $item_output = sprintf( '%1$s<a data-scroll %2$s>%3$s%4$s%5$s</a>%6$s',
                               $args->before,
                               $attributes,
                               $args->link_before,
                               apply_filters( 'the_title', $item->title, $item->ID ),
                               $args->link_after,
                               $args->after
                              );

        // build html
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
}

?>
