<!DOCTYPE html>
<html <?php language_attributes() ?>>
    <head>
        <meta charset="<?php bloginfo('charset') ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link href="//www.google-analytics.com" rel="dns-prefetch" />
        <link href="<?php echo get_template_directory_uri(); ?>/favicon.ico" rel="shortcut icon" />
        <meta name="robot" content="NOODP, INDEX, FOLLOW" />
        <meta name="author" content="Lluvia de Obsequios" />
        <meta name="language" content="VE" />
        <meta name="geo.position" content="10.333333;-67.033333" />
        <meta name="ICBM" content="10.333333, -67.033333" />
        <meta name="geo.region" content="VE" />
        <meta name="geo.placename" content="ubicacion" />
        <meta name="DC.title" content="<?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?>" />
        <title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
        <meta name="title" content="<?php if(is_home()) { echo bloginfo("name"); echo " | "; echo bloginfo("description"); } else { echo wp_title(" | ", false, 'right'); echo bloginfo("name"); } ?>">
        <?php $current_url = $_SERVER['REQUEST_URI']; $clean_url = str_replace('/', '', $current_url); ?>
        <?php if(is_single()) : $the_slug = $clean_url; $args=array('name' => $the_slug, 'posts_per_page' => 1); $my_posts = get_posts( $args ); ?>
        <?php if( $my_posts ) { $excerpt = $my_posts[0]->post_excerpt; echo '<meta name="description" content="' . htmlentities($excerpt, ENT_QUOTES, 'UTF-8') . '" />'; } ?>
        <?php endif; ?>
        <?php if (is_home() ) { echo '<meta name="description" content="'. get_bloginfo("name") .' | ' . get_bloginfo("description") . '">'; } ?>
        <?php if (is_page() ) { echo '<meta name="description" content="'. get_bloginfo("name") .' | ' . get_bloginfo("description") . '">'; } ?>
        <?php if(is_single()) : $the_slug = $clean_url; $args=array('name' => $the_slug, 'posts_per_page' => 1); $my_posts = get_posts( $args ); ?>
        <?php if( $my_posts ) { $terms = get_the_terms( $my_posts[0]->ID, 'post_tag' ); if ( $terms && ! is_wp_error( $terms ) ) : $draught_links = array(); ?>
        <?php foreach ( $terms as $term ) { $draught_links[] = $term->name; } $on_draught = join( ", ", $draught_links ); endif;
                               echo '<meta name="keywords" content="KEYWORDS , '. $on_draught .'" />'; } ?>
        <?php endif; ?>
        <?php if (is_home() || is_page() ) { echo '<meta name="keywords" content="KEYWORDS" />'; } ?>
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:site" content="@AUTHOR" />
        <meta name="twitter:creator" content="@AUTHOR" />
        <meta property='fb:admins' content='100000133943608' />
        <meta property="fb:app_id" content="611067105660122" />
        <meta property="og:title" content="<?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?>" />
        <meta property="og:site_name" content="PROYECTO" />
        <meta property="og:type" content="article" />
        <meta property="og:locale" content="es_ES" />
        <meta property="og:url" content="<?php if(is_single()) { the_permalink(); } else { echo 'PROYECTO'; }?>" />
        <?php if(is_single()) : $the_slug = $clean_url; $args=array('name' => $the_slug, 'posts_per_page' => 1); $my_posts = get_posts( $args ); ?>
        <?php if( $my_posts ) { $excerpt = $my_posts[0]->post_excerpt; echo '<meta property="og:description" content="' . htmlentities($excerpt, ENT_QUOTES, 'UTF-8') . '" />'; } ?>
        <?php endif; ?>
        <?php if (is_home() ) { echo '<meta property="og:description" content="'. get_bloginfo("name") .'" | "' . get_bloginfo("description") . '">'; } ?>
        <?php if (is_page() ) { echo '<meta property="og:description"  content="'. get_bloginfo("name") .'" | "' . get_bloginfo("description") . '">'; } ?>
        <meta property="og:image" content='<?php if(is_single()){ $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); echo $url; } else { echo esc_url( get_template_directory_uri() ) ."/images/oglogo.png"; } ?>' />
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php wp_head() ?>
        <!--[if IE]> <script src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script> <![endif]-->
        <!--[if IE]>  <script src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script> <![endif]-->
        <script type="text/javascript">
            var ruta_blog = "<?php echo esc_url(get_template_directory_uri()); ?>";
        </script>
    </head>
    <body <?php body_class() ?>>
        <div id="fb-root"></div>
        <header id="top" class="container-fluid">
            <div class="row">
                <div id="sticker" class="the-header col-md-12 no-paddingl no-paddingr">
                    <div class="row visible-md visible-lg">
                        <div class="logo-container col-md-2 col-sm-2 col-xs-2">
                            <a href="<?php echo home_url('/'); ?>">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="Lluvia de Regalos Logo" class="img-responsive" />
                            </a>
                        </div>
                        <div class="menu-container col-md-7 col-sm-7 col-xs-7">
                            <div class="login-container col-md-12">
                                <?php if ( is_user_logged_in() ) { $current_user = wp_get_current_user(); ?>
                                <span> <?php echo $current_user->user_login; ?> <i class="fa fa-user"></i><a href="<?php echo home_url('/mi-cuenta'); ?>"> <i class="fa fa-cog"></i></a></span>
                                <?php } else { ?>
                                <span><a href="<?php echo home_url('/mi-cuenta'); ?>">Iniciar Sesión</a></span>
                                <?php } ?>
                            </div>
                            <div class="navbar-container col-md-12 no-paddingr">
                                <nav class="navbar navbar-default">
                                    <div class="container-fluid">
                                        <!-- Brand and toggle get grouped for better mobile display -->
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>

                                        </div>

                                        <!-- Collect the nav links, forms, and other content for toggling -->
                                        <?php wp_nav_menu( array( 'theme_location' => 'header_menu', 'depth' => 2, 'container' => 'div',
                                                                 'container_class' => 'collapse navbar-collapse', 'container_id' => 'bs-example-navbar-collapse-1', 'menu_class' => 'nav navbar-nav navbar-right', 'fallback_cb' => 'wp_bootstrap_navwalker::fallback', 'walker' => new wp_bootstrap_navwalker() )); ?>
                                    </div><!-- /.container-fluid -->
                                </nav>
                            </div>
                        </div>
                        <div class="wishlist-container col-md-3 col-sm-3 col-xs-3">
                            <a id="wishlist-btn" href="<?php echo home_url('/mi-lista-de-deseos'); ?>" title="Lista de Deseos">
                                <?php if ( is_user_logged_in() ) { ?>
                                <?php global $wpdb; $current_user = wp_get_current_user(); ?>
                                <?php $wish_count = $wpdb->get_var( $wpdb->prepare("SELECT COUNT(*) FROM wp_yith_wcwl WHERE user_id = %s", $current_user->ID)); ?>
                                <?php } else { ?>
                                <?php $cookie = $_COOKIE['yith_wcwl_products']; ?>
                                <?php $cookie = stripslashes($cookie); $variable = json_decode($cookie, true); ?>
                                <?php $wish_count = count($variable); }?>
                                <span class="sprites-wishcart sprites-wish">
                                    <?php if ($wish_count != 0) { ?>
                                    <span class="wishcart-badge"><?php echo $wish_count; ?></span>
                                    <?php } ?>
                                </span>
                            </a>
                            <a href="<?php echo home_url('/carro'); ?>" title="Carrito de compras">
                                <span class="sprites-wishcart sprites-cart">
                                    <?php global $woocommerce; $count = $woocommerce->cart->cart_contents_count; ?>
                                    <span class="cart-content"><?php echo $count; ?></span>
                                </span>
                            </a>
                        </div>
                    </div>
                    <div class="row visible-sm visible-xs">
                        <div class="logo-container col-md-3 col-sm-3 col-xs-3">
                            <a href="<?php echo home_url('/'); ?>">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="Lluvia de Regalos Logo" class="img-responsive" />
                            </a>
                        </div>
                        <div class="menu-container col-md-9 col-sm-9 col-xs-9">
                            <div class="login-container col-md-12">
                                <?php if ( is_user_logged_in() ) { $current_user = wp_get_current_user(); ?>
                                <span> <?php echo $current_user->user_login; ?> <a href="<?php echo home_url('/mi-cuenta'); ?>"><i class="fa fa-user"></i> <i class="fa fa-cog"></i></a></span>
                                <?php } else { ?>
                                <span><a href="<?php echo home_url('/mi-cuenta'); ?>">Iniciar Sesión</a></span>
                                <?php } ?>


                                <a href="<?php echo home_url('/mi-lista-de-deseos'); ?>" title="Lista de Deseos">
                                    <span class="sprites-wishcart sprites-wish"></span>
                                </a>
                                <a href="<?php echo home_url('/carro'); ?>" title="Carrito de compras">
                                    <span class="sprites-wishcart sprites-cart">
                                        <?php global $woocommerce; $count = $woocommerce->cart->cart_contents_count; ?>
                                        <span class="cart-content"><?php echo $count; ?></span>
                                    </span>
                                </a>
                            </div>
                            <div class="navbar-container col-md-12 no-paddingr">
                                <nav class="navbar navbar-default">
                                    <div class="container-fluid">
                                        <!-- Brand and toggle get grouped for better mobile display -->
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>

                                        </div>

                                        <!-- Collect the nav links, forms, and other content for toggling -->
                                        <?php wp_nav_menu( array( 'theme_location' => 'header_menu', 'depth' => 2, 'container' => 'div',
                                                                 'container_class' => 'collapse navbar-collapse', 'container_id' => 'bs-example-navbar-collapse-2', 'menu_class' => 'nav navbar-nav navbar-right', 'fallback_cb' => 'wp_bootstrap_navwalker::fallback', 'walker' => new wp_bootstrap_navwalker() )); ?>
                                    </div><!-- /.container-fluid -->
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>


